import React, { useRef, useEffect, memo } from 'react';
import { VariableSizeList as List, areEqual } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';

const Row = memo(({
  component:Component,
  data, index,
  style,
  listWidth,
  setHeight,
  getSize
}) => {
  const containerRef = useRef();

  useEffect(() => {
    const container = containerRef.current;
    const { height } = container.getBoundingClientRect();
    if (getSize(index) !== height) {
      setHeight(index, height);
    }
  }, [ listWidth, setHeight, index, getSize ])

  return (
    <div style={style}>
      <div ref={containerRef} >
        <Component data={data[index].data} />
      </div>
    </div>
  )
}, areEqual)

const VirtualScrollList = ({ component, data }) => {
  const listRef = React.useRef();

  const sizeMap = React.useRef({});
  const getSize = React.useCallback(index => sizeMap.current[index] || 50, []);
  const setSize = React.useCallback((index, size) => {
    sizeMap.current = { ...sizeMap.current, [index]: size };
    if (listRef.current) listRef.current.resetAfterIndex(index);
  }, []);

  return (
    <AutoSizer>
      {({ height, width }) => (
        <List
          itemCount={data.length}
          itemData={data}
          itemSize={getSize}
          width={width}
          height={height}
          ref={listRef}
        >
          {({ index, style }) => (
              <Row
                key={index}
                index={index}
                data={data}
                style={style}
                listWidth={width}
                getSize={getSize}
                setHeight={setSize}
                component={component}
              />
          )}
        </List>
      )}
    </AutoSizer>
  );
};

export default VirtualScrollList;
