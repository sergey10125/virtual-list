import React, { forwardRef } from 'react';
import { DynamicSizeList as List, areEqual } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';

// const Row = ({ component:Component, data, style }) => {
//   console.log(data);
  
//   return (
//     <div style={style}>
//       <Component data={data.data} />
//     </div>
//   )
// }

// const Row = ({ index, style }) => (
//   <div style={style}>{index}</div>
// );

const VirtualScrollList = ({ component: Component, data }) => {

  const RefForwarder = forwardRef(({ index, style }, ref) => (
    <div style={style} ref={ref}>
      <Component data={data[index].data} />
    </div>
  ));

  return (
    <AutoSizer>
      {({ height, width }) => (
        <List
          itemCount={data.length}
          width={width}
          height={height}
        >
          {RefForwarder}
        </List>
      )}
    </AutoSizer>
  );
};

export default VirtualScrollList;
