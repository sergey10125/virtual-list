import React, { FC, memo } from 'react';
import VirtualScrollList from './list-virtualized'
import VirtualScrollListNext from './list-virtualized-next'
import './App.css';

type CompProps = {
  data: {
    strings: string[]
  }
}

const Comp:FC<CompProps> = memo(({ data, ...rest }) => {
  const { strings } = data;
  return (
    <div style={{ paddingBottom: '15px' }}>
      <hr/>
      {strings.map(str => (
        <div>{str}</div>
      ))}
    </div>
  )
})

const data = new Array(10000).fill(null).map((_, i) =>({
  data: {
    strings: new Array(Math.round(Math.random() * 7) + 1)
      .fill(null)
      .map((_, j) => {
        const space = new Array(Math.round(Math.random() * 70)).fill('-').join('-');
        return `${space}| item ${i}-${j} |${space}`;
      })
  }
}))

function App() {
  return (
    <div className="App">
      {/* <VirtualScrollListNext component={Comp} data={data} /> */}
      <VirtualScrollList component={Comp} data={data} />
    </div>
  );
}

export default App;
